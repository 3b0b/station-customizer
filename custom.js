// Fetch our CSS in parallel ahead of time
const cssPath = 'https://raw.githubusercontent.com/Nockiro/slack-black-theme/master/custom.css';
let cssPromise = fetch(cssPath).then(response => response.text());

let customConfigCSS = `
:root {
    /* Modify these to change your theme colors: */
    --primary: #FFF;
    --text: #CCC;
    --background: #225;
    --background-elevated: #225;
    --background-light: #225;
    --background-bright: #225;
    --background-hover: #1142;
}
`

cssPromise.then(css => {
    if (document.querySelectorAll('.appcues-subdock-recent')) {
            let s = document.createElement('style');
            s.type = 'text/css';
            s.innerHTML = '.appcues-subdock-recent, .appcues-subdock-recent>* {height: 0px;margin:0px;}';
            document.head.appendChild(s);
        let darkener = `
            let s = document.createElement('style');
            s.type = 'text/css';
            s.id = 'slack-custom-css';
            s.innerHTML = \`${css + customConfigCSS}\`;
            document.head.appendChild(s);
        `
        window.darkenerInterval = setInterval(function darkenSlacks() {
            document.querySelectorAll(".l-webview__tab webview[src*='slack.com']:not([darkened])").forEach(webview => {
                console.log('darkening '+webview.localName)
                webview.executeJavaScript(darkener);
                webview.darkened = true;
                return darkenSlacks;
            })
        }(),300000)
    }
    else {
        let s = document.createElement('style');
        s.type = 'text/css';
        s.id = 'slack-custom-css';
        s.innerHTML = css + customConfigCSS;
        document.head.appendChild(s);
    }
});